<li class="Item">
    <a href="/good">
        <div class="JS-Image-Align View" data-image-ratio='1/1' data-image-position='center/top'>
            <img src="/img/item-1.jpg">
        </div>
        <h2>
            <span>Xilin DB 2000</span>
        </h2>

        <div class="Info">
            <div class="Capacity">
                <svg>
                    <use xlink:href="#arrow-load"></use>
                </svg>
                <svg>
                    <use xlink:href="#load"></use>

                </svg>

                <span>2000кг</span>
            </div>
            <div class="Weight">
                <svg>
                    <use xlink:href="#arrow-load"></use>
                </svg>
                <svg>
                    <use xlink:href="#load"></use>
                </svg>
                <span>55кг</span>
            </div>
        </div>

    </a>
</li>
<li class="Caption"></li>