<header class="Site-Header">
    <a href="/" class="Logo">
        <svg>
            <use xlink:href="#logo"></use>
        </svg>
    </a>
    <ul class="Links">
        <li class="JS-Current-Links"><a href="/catalog">Каталог</a></li>
        <li class="JS-Current-Links"><a href="/news">Новости</a></li>
        <li data-scroll-point-duration='600' ><a class="JS-Scroll-Button" href="/#about">О компании</a></li>
        <li class="JS-Current-Links"><a href="/contacts">Контакты</a></li>
        <li class="Vk"><a href="">
                <svg>
                    <use xlink:href="#vk"></use>
                </svg>
            </a></li>
    </ul>
    <ul class="Phones">
        <li><span>+7 (4922)</span>44-75-85</li>
        <li><span>+7 (4922)</span>45-03-26</li>
    </ul>
    <ul class="Hamburger">
        <li></li>
        <li></li>
        <li></li>
    </ul>
</header>
