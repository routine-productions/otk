<div class="Catalog">
    <nav>
        <ul class="Catalog-Nav">
            <li class="JS-Current-Links"><a href="">Штабелёры</a></li>
            <li class="JS-Current-Links"><a href="">Погрузчики</a></li>
            <li class="JS-Current-Links"><a href="">Телеги</a></li>
            <li class="JS-Current-Links"><a href="">Шины</a></li>
            <li class="JS-Current-Links"><a href="">Колеса</a></li>
            <li class="JS-Current-Links"><a href="">Стропы</a></li>
        </ul>
    </nav>
    <ul class="Catalog-List">
        <!--Require-Item x12-->
        <?php for ($index = 0; $index < 12; $index++)
        {
            require __DIR__ . "/../modules/item.php";
        }
        ?>

    </ul>
    <?php
    require __DIR__ . "/../modules/pagination.php";
    ?>
</div>


<dl>
    <dt>Грузоподъемность</dt>
    <dd>2650 кг</dd>
</dl>
<dl>
    <dt>Масса</dt>
    <dd>76 кг</dd>
</dl>
<dl>
    <dt>Длина вилки</dt>
    <dd>150 см</dd>
</dl>
<dl>
    <dt>ширина вилки</dt>
    <dd>60 см</dd>
</dl>
<dl>
    <dt>Материалл колес</dt>
    <dd>Резина/металл</dd>
</dl>
<dl>
    <dt>Диаметр колёс</dt>
    <dd>18 см</dd>
</dl>