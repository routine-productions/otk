<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <title>ОТК</title>

    <link
        href='https://fonts.googleapis.com/css?family=Exo+2:400,200italic,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic&subset=latin,cyrillic,latin-ext'
        rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/css/index.min.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>
<?php require __DIR__ . "/img/sprite.svg"; ?>

<?php

require_once __DIR__ . '/libs/Mobile_Detect.php';
$Detect = new Mobile_Detect;
$Dir_Images = __DIR__ . '/images/';
//?>

<div class="Header-Wrap">
    <?php require_once __DIR__ . '/modules/header.php'; ?>
</div>
<main>
    <?php
    if ($_SERVER['REQUEST_URI'] == '/')
    {
        require __DIR__ . '/pages/company.php';
    } else
    {
        require __DIR__ . '/pages/' . str_replace('layouts_1/', '', $_SERVER['REQUEST_URI']) . '.php';
    }
    ?>
    <div class="Go-Top JS-Scroll-Top" data-scroll-duration='300'>
        <svg>
            <use xlink:href="#arrow"></use>
        </svg>
    </div>
</main>
<footer class="Site-Footer">
    <?php require_once __DIR__ . '/modules/footer.php'; ?>
</footer>

<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="index.min.js"></script>

<body>
</html>