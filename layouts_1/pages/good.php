    <section class="Good">
        <h2 class="Good-Title Mobile">Noblift AC20
            <small>Телега гидравлическая</small>
        </h2>
        <div class="Good-Views">
            <div class="JS-Image-Align Large" data-image-ratio='1/1' data-image-position='center/top'>
                <img src="/img/good-view-1.jpg">
            </div>
            <div class="View-Small">
                <div class="JS-Image-Align Small" data-image-ratio='4/3' data-image-position='center/top'>
                    <img src="/img/good-view-1.jpg">
                </div>
                <div class="JS-Image-Align Small" data-image-ratio='4/3' data-image-position='center/top'>
                    <img src="/img/good-view-1.jpg">
                </div>
                <div class="JS-Image-Align Small" data-image-ratio='4/3' data-image-position='center/top'>
                    <img src="/img/good-view-1.jpg">
                </div>
            </div>
        </div>
        <section class="Good-Kinds">
            <h2 class="Good-Title Desktop">Noblift AC20
                <small>Телега гидравлическая</small>
            </h2>
            <h2>Технические характеристики</h2>
            <dl>
                <dt>Грузоподъемность</dt>
                <dd>2650 кг</dd>
            </dl>
            <dl>
                <dt>Масса</dt>
                <dd>76 кг</dd>
            </dl>
            <dl>
                <dt>Длина вилки</dt>
                <dd>150 см</dd>
            </dl>
            <dl>
                <dt>ширина вилки</dt>
                <dd>60 см</dd>
            </dl>
            <dl>
                <dt>Материалл колес</dt>
                <dd>Резина/металл</dd>
            </dl>
            <dl>
                <dt>Диаметр колёс</dt>
                <dd>18 см</dd>
            </dl>
        </section>
    </section>

    <section class="Similar">
        <h2 class="Similar-Title">Похожие товары</h2>
        <ul class="Similar-List">
            <!--Require-Item x4-->
            <?php for ($index = 0; $index < 4; $index++) {
                require __DIR__ . "/../modules/item.php";
            }
            ?>
        </ul>
    </section>

        <nav class="Pagination">
            <span>Страница:</span>
            <ul class="pageList">[[!+page.nav]]</ul>
            <div class="clear"></div>
        </nav>