<ul class="Footer-Navigation">
    <li class="JS-Current-Links"><a href="/catalog">Каталог</a></li>
    <li class="JS-Current-Links"><a href="/services">Услуги</a></li>
    <li class="JS-Current-Links"><a href="/contacts">Контакты</a></li>
    <li class="JS-Current-Links"><a href="/#about">О компании</a></li>
</ul>

<div class="Copyright">
    <ul class="Otk">
        <li>© 2016 ООО «ОТК»</li>
        <li>Продажа и аренда спецтехники для складов</li>
    </ul>
    <ul class="Made-By">
        <li>Разработка <a href="http://progress-time.ru/">Bunker.Lab</a></li>
        <li>Продвижение <a href="http://progress-time.ru/">Progress-Time</a></li>
    </ul>
</div>