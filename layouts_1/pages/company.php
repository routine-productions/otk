<div class="Top-Wrap">
    <div class="Top">
        <div class="Top-Content">
            <h1>
                Специализированный магазин
                складского оборудования
                и погрузочной техники
                во Владимире
            </h1>
            <button href="#catalog" class="JS-Scroll-Button">Перейти к нашему каталогу</button>
        </div>
    </div>
</div>

<div id="catalog" class="Company">
    <section class="Company-Catalog">
        <h2>
            Каталог товаров
        </h2>
        <ul class="List">
            <li class="Item">
                <a href="/catalog">
                    <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                        <img src="/img/pic-1.jpg">
                    </div>

                    <h3><span>Штабелёры</span></h3>
                </a>
            </li>
            <li class="Caption"></li>
            <li class="Item">
                <a href="/catalog">
                    <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                        <img src="/img/pic-2.jpg">
                    </div>
                    <h3><span>Погрузчики</span></h3>
                </a>

            </li>
            <li class="Caption"></li>
            <li class="Item">
                <a href="/catalog">
                    <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                        <img src="/img/pic-3.jpg">
                    </div>

                    <h3><span>Телеги</span></h3>
                </a>

            </li>
            <li class="Caption"></li>
            <li class="Item">
                <a href="/catalog">
                    <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                        <img src="/img/pic-4.jpg">
                    </div>

                    <h3><span>Шины для спецтехники</span></h3>
                </a>

            </li>
            <li class="Caption"></li>
            <li class="Item">
                <a href="/catalog">
                    <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                        <img src="/img/pic-5.jpg">
                    </div>

                    <h3><span>Колеса для тележек</span></h3>
                </a>

            </li>
            <li class="Caption"></li>
            <li class="Item">
                <a href="/catalog">
                    <div class="JS-Image-Align View" data-image-ratio='4/3' data-image-position='center'>
                        <img src="/img/pic-6.jpg">
                    </div>

                    <h3><span>Стропы</span></h3>
                </a>
            </li>
            <li class="Caption"></li>
        </ul>
        <a class="Go-To" href="/catalog">Перейти к каталогу</a>
    </section>
    <section class="Company-News">
        <h2>
            Новости компании
        </h2>
        <ul class="List">
            <?php for ($index = 0; $index < 3; $index++)
            {
                require __DIR__ . "/../modules/new-module.php";
            }
            ?>
        </ul>

        <a class="Go-To" href="/news">Перейти к новостям</a>
    </section>
    <section id="about" class="Company-About">
        <h2>О компании «ОТК»</h2>
        <article>
            <h4>
                Компания ООО "ОТК - 33" существует уже более 8 лет на Владимирском рынке складского оборудования и
                погрузочной техники.
            </h4>
            <p>
                Мы зарекомендовали себя как надежные партнеры в данной сфере деятельности. Среди наших клиентов
                крупные предприятия Владимира и Владимирской области, а также предприятия малого и среднего бизнеса.
            </p>

            <p>
                В городе Владимире работает специализированный магазин, в котором представлен широкий ассортимент
                гидравлических и платформенных телег, а также штабелеров ведущих мировых брендов, различных колес и
                роликов итальянской фирмы TALLURE ROTA для телег, а также широкий спектр запасных частей и резины
                для различных марок погрузчиков (Япония, Китай, Болгария).
            </p>
        </article>

        <article>
            <h3>
                Гарантийное обслуживание и ремонт спецтехники
            </h3>
            <p>С 2005 года наша фирма является официальным сертифицированным партнером компании HELI по Владимиру и
               Владимирской области. Мы не только поставляем технику компании HELI, но и проводим гарантийное и
               пост гарантийное техническое обслуживание. В нашей сервисной службе работают специалисты высокого
               класса, а также работает специализированная выездная сервисная бригада. Мы производим ремонт
               погрузчиков любой сложности. </p>
        </article>
    </section>
</div>
