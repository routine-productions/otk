<div class="Contacts">
    <div class="Map">
        <script type="text/javascript" charset="utf-8"
                src="https:/api-maps.yandex.ru/services/constructor/1.0/js/?sid=vlkvLJoaK8FB6Cz1K-aEPxjLcNci35pu&width=100%&height=550&lang=ru_UA&sourceType=constructor"></script>
    </div>
    <div class="Contacts-Info">
        <h2>Контактная информация</h2>
        <dl>
            <dt>Режим-работы</dt>
            <dd>
                <p>Понедельник - Пятница</p>

                <p> 8:00 - 18:00</p>
            </dd>
            <dt>Адрес</dt>
            <dd>ул. Полины Осипенко , д. 57
                г. Владимир, Владимирская обл.
                Россия
            </dd>
            <dt>Телефоны и почта</dt>
            <dd class="Info-Mail">
                <span>e-mail:</span><span><a href="mail-to: otk33@mail.ru">otk33@mail.ru</a></span>
                <span>icq:</span><span> 435146614</span>


            </dd>
        </dl>
    </div>
    <form class="Contacts-Form" action="">
        <h2>Напишите нам</h2>

        <div><label for="name">Имя</label><input id="name" type="text"/></div>
        <div><label for="phone">Телефон или e-mail</label><input id="phone" type="text"/></div>
        <div><label class="Align" for="message">Сообщение</label><textarea id="message"></textarea></div>
        <button>Отправить сообщение</button>
    </form>

</div>